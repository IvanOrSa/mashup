var translateApi=(function() {
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////Cambia a tus credencialesbueno tu KeyApi////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  var key_api = "AIzaSyA4H32a9KA1YnrvOIvwlyi4RfJFLMTjLy4";
  var URL_Youtube="https://www.googleapis.com/youtube/v3/search?";
  var URL_Video="https://www.googleapis.com/youtube/v3/videos?";
  var URL_Maps="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4H32a9KA1YnrvOIvwlyi4RfJFLMTjLy4&callback=initMap"  
  var maxResultados;//Cantidad de videos a pedir
  var textSearch;//Texto a buscar, traducido
  var arrVideosID=[];//arreglo de id´s
  var arregloDe10;
  var indice=0;
  var map;
  //var api_key_twitter="b0sRlZmF6UxxAUAwZLmxDKaPY";
  //var api_secret_twitter="pHlYAMvgGKOiFTwlV9NkmKycjVepl1SKsRK5iThrON9YJqd9cI";
  //var acces_token="983132903797350400-kO3Gjph5qMPkFUXzRRBfumFICYKOtDh";
  //var acces_token_secret="0TuvVCfXnkdDPPMQgckGVJkF2kXvtxry3fMYQZtLtKOYr";
  
   //FUNCION GENERAL PARA HACER PETICIONES A GOOGLE CLOUD PLATFORM, APIS DE GOOGLE
  var _descargar=function (metodo,requestJson,url,funcion) {
    var xhr=new XMLHttpRequest();
    xhr.onreadystatechange= function () {
      if (xhr.readyState=== 4) {
        if (xhr.status===200) {
          //document.write("todo chingon");
          //document.write(this.responseText);
          funcion(this.responseText);
          //console.log(this.responseText);
          
        }else{
          document.write("Algo salió mal");
          //document.write("Creo que no hay internet :( ");
         }
      }

    };
    xhr.open(metodo ,url,true);
    xhr.send(requestJson);
  };

  
/// API Youtube Data

 //ARMAR LA URL PARA LA PETICION DE LA BUSQUEDA DE VIDEOS youtube search
 var _armarURLYoutube=function(maxResultados,texto,nextToken){
    var url=URL_Youtube
        +"&part=snippet"
        +"&maxResults="+maxResultados
        +"&order=date"
        +"&q="+texto
        +"&type=video"
        +"&key="+key_api
        if(nextToken){
           url+="&pageToken="+nextToken
          }
        //console.log(url);
        return url; 
  };
  //Armar URL para peticion de video con id
  var _armarURLVideo=function(videoIds) {
    var url=URL_Video
      +"part=snippet,recordingDetails, statistics"
      +"&id="+videoIds
      +"&key="+key_api;
      return url;
  }

  //Verificacion si hay texto y cantidad de videos requeridos
  var _verificarCantidadBusqueda=function(){
    maxResultados=document.getElementById("cantidadVideos").value;
    textSearch=document.getElementById("areaPintar").value;
    //inicializando el arreglo 
    arrVideosID=[];
    
    
      //console.log(textSearch);
      if (textSearch) {
        if (maxResultados!==NaN && maxResultados>0) {
          //Agregando funcionalidad de recaptcha, si no lo han verificado, no procesa resultado
          if(_verificaRecaptcha()){
           _procesaCantidadVideos("");
           _initMap();
          }else{alert("Captcha no verificado")}
          
        }else alert("Verifica cantidad de videos");
      }else alert("No hay texto a buscar");
  };


 //Procesar peticion mayor a 50 videos o menor a 50 videos
 var _procesaCantidadVideos=function(nextToken) {
      //console.log(maxResultados);
      if (maxResultados>0) {
            if (maxResultados<=50) {
            //console.log(maxResultados);
            _descargar("GET","",_armarURLYoutube(maxResultados,textSearch),_procesarResultadoYT);
            }else{
              maxResultados-=50;
              _descargar("GET","",_armarURLYoutube(50,textSearch,nextToken),_procesarMasDe50);
            }
          }
  }; 
 //paginar mas de 50
  var _procesarMasDe50=function(ArrJSON) {
    var respuestaM5=JSON.parse(ArrJSON);
    for (var i = respuestaM5.items.length - 1; i >= 0; i--) {
        var videoId=respuestaM5.items[i].id.videoId;
         arrVideosID.push(videoId);
        //console.log(videoId);
      }
      //console.log(arrVideosID);
      if (respuestaM5.nextPageToken) {
        //console.log(respuestaM5.nextPageToken);
        _procesaCantidadVideos(respuestaM5.nextPageToken);
     }else
        _procesarResultadoYT();
    };
 

 //procesa resultado de la busqueda
  var _procesarResultadoYT=function (ArrJSON) {
    var respuesta=JSON.parse(ArrJSON);
    //var arrVideoId=new Array();
    for (var i = respuesta.items.length - 1; i >= 0; i--) {
        var videoId=respuesta.items[i].id.videoId;
        arrVideosID.push(videoId);
        //console.log(videoId);
      }
      //console.log(arrVideosID);
      _paginacion();
  };
  //paginar videos de 10 en 10
  var _paginacion=function(){
    var cantidad_arreglos10=Math.ceil(arrVideosID.length/10);
    arregloDe10=new Array(cantidad_arreglos10);
    var indiceinterno=0; 
    for (var i =0; i <cantidad_arreglos10; i++) {
         var arreInterno=[];
         for (var j =0; j < 10; j++){
          if (typeof arrVideosID[indiceinterno]==="undefined") {

          }else{
          arreInterno.push(arrVideosID[indiceinterno]);
          indiceinterno++;
        }
        }
        arregloDe10[i]=arreInterno;
      }
      //console.log(arregloDe10);
      _crearReproductor(0);
     
      
    };

  //cargar videos paginados next
  var _cargaVideosNext=function(){
    if (arregloDe10.length!==0) {
      indice+=1;
      if (indice>= arregloDe10.length) {
        indice=arregloDe10.length-1;
        //console.log("indice pos: "+ indice );
      }else{
      //console.log("indice pos: "+ indice );
      _crearReproductor(indice);
      }
    }else console.log('No hay videos que mostrar');
      
  };
  //cargar videos paginados before
  var _cargarVideosBefore=function(){
    if (arregloDe10.length!==0) {
        indice-=1;
        if (indice<0) {
            indice=0;
            //console.log("indice pos: "+ indice );
        }else{
      //console.log("indice pos: "+ indice );
      _crearReproductor(indice);
     }
  }else console.log('No hay videos para mostrar');
    
  };
  //CREA LOS IFRAME DONDE SE INCRUSTAN LOS VIDEOS 
  var _crearReproductor=function(indice) {
    console.log(indice);
    var contenedorVideo=document.getElementById("videoYoutube");
    //console.log(contenedorVideo);
    var url='https://www.youtube.com/embed/';
    
    //limpiando frame de video
    while (contenedorVideo.firstChild) {
      contenedorVideo.removeChild(contenedorVideo.firstChild);
    }
    //creando frames de videos
    for (var i = 0; i<arregloDe10[indice].length; i++) {
      //frameVideo.onYouTubeIframeAPIReady(arrVideoId[i]);
      var newLi=document.createElement("li");
        newLi.setAttribute("class","flex-item");
      var frame=document.createElement("iframe");
        frame.setAttribute("id","frameVideo");
        //frame.setAttribute("width","640");
        //frame.setAttribute("height","360");
        frame.setAttribute('src',url+arregloDe10[indice][i]); 
        newLi.append(frame);
      contenedorVideo.append(newLi);
    }
  };
  

//Buscar videos con ubicación armar cadena de id´s para pasarlo a maps
  var _geolocalizacion=function() {
    //console.log(arrVideosID.length);
     _buscarTweets(textSearch);
    if (arrVideosID.length!==0 ) {
        //console.log(arrVideosID);
      var ids_vd_yt="";
      for (var i =0; i <arrVideosID.length; i++) {
            if (i<arrVideosID.length-1) 
              ids_vd_yt+=arrVideosID[i]+",";
            else
              ids_vd_yt+=arrVideosID[i];
      }
      //console.log(ids_vd_yt);
      _descargar("GET","",_armarURLVideo(ids_vd_yt),_validarGeolocalizacion);    
 
     }else alert("No se han buscado videos \n No se han buscado tweets");
   };

  var _validarGeolocalizacion = function(ArrJSONVideos){
    var videos = JSON.parse(ArrJSONVideos);
    //console.log(videos);
     
     //se comento para probar iniciar mapa en funcion init map
     /* var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 3,
                center: {lat: 30.2513675, lng: -33.7050631}
            });*/
     
    
    for (var i = 0 ; i < videos.items.length; i++) {
      if(videos.items[i].recordingDetails){
        if(videos.items[i].recordingDetails.location){
          if(videos.items[i].recordingDetails.location.latitude && videos.items[i].recordingDetails.location.longitude){
             //_initMap(videos.items[i].recordingDetails.location.latitude,videos.items[i].recordingDetails.location.longitude, videos.items[i].id);
              var contentString = '<div><iframe class="col-xs-8 col-sm-6 col-md-12" src=\"https://www.youtube.com/embed/'+videos.items[i].id+'\?autoplay=1"></iframe></div>';
              console.log("https://www.youtube.com/embed/"+videos.items[i].id);
              _punterMapa({ lat: videos.items[i].recordingDetails.location.latitude ,
                          lng: videos.items[i].recordingDetails.location.longitude },contentString);
            
          }        
        }        
      }
    }
 };


////////Google maps
  var _initMap=function() {
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 3,
                center: {lat: 30.2513675, lng: -33.7050631}
            });
                    
  };
  var _punterMapa=function(coordenadas,frame,icono){
      var infowindow = new google.maps.InfoWindow({
             content: frame
        });
      var marker = new google.maps.Marker({
            position: coordenadas,
            map: map,
            icon:icono
            //title: videos.items[i].snippet.title
        });


          marker.addListener('click', function() {
             infowindow.open(map, marker);
          });
  };
 



  ////////////////Twitter Hace peticion a mi servidor puesto en /routes/index.js
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////Cambia a tu URL de heroku para que funcione////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  var _buscarTweets=function(texto) {
    
  $.ajax({
    url:"http://mashup-orsa.herokuapp.com/twitter",
    method:"GET",
    data:{
      "texto":texto
        }
      }).done(function(response){
        
        for (var i =0; i<100; i++) {
          //console.log(response[0]);
          if (response[0].statuses[i].geo) {
              console.log(response[0].statuses[i].geo);
              var datos = {"titulo": response[0].statuses[i].user.name,
                           "parrafo": response[0].statuses[i].text,
                           "imagen": response[0].statuses[i].user.profile_image_url};
              var card = _cardTweet(datos);
              var geo = {lat: response[0].statuses[i].geo.coordinates[0],
                         lng: response[0].statuses[i].geo.coordinates[1]};


              _punterMapa(geo,card,"http://maps.google.com/mapfiles/ms/icons/blue-dot.png");
          }else{
            console.log("no tiene coordenadas");
          } 
        }
     });
  };


//Creando tarjeta de presentación de twitter
  var _cardTweet=function(datos){
                var row = document.createElement("row");
                var divCard = document.createElement("div");
                divCard.setAttribute("class", "col-md-12");
                var card = document.createElement("div");
                card.setAttribute("class", "p-3 mb-2 bg-info text-white");
                var cardContent = document.createElement("div");
                cardContent.setAttribute("class","p-3 mb-2 bg-primary text-white");
                var titulo = document.createElement("span");
                var imagen = document.createElement("img");
                imagen.setAttribute("src",datos.imagen);
                cardContent.appendChild(imagen);
                titulo.setAttribute("class","card-title");
                titulo.innerHTML = datos.titulo;
                var parrafo = document.createElement("p");
                parrafo.innerHTML = datos.parrafo;
                cardContent.appendChild(titulo);
                cardContent.appendChild(parrafo);
                card.appendChild(cardContent);
                divCard.appendChild(card);
                row.appendChild(divCard);
      return row;
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////pruebas//////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Verificar recaptcha
var _verificaRecaptcha=function(){
  var response = grecaptcha.getResponse();
        if(response.length == 0){
          alert("Captcha no verificado")
          return false;
        } else {
          //alert("Captcha verificado");
          return true;
        }
}

  var _iniciar=function() {
        
      //BUSCAR EN YOUTUBE
      var buscar=document.getElementById("search");
      buscar.addEventListener('click',_verificarCantidadBusqueda,false);
      //buscar.addEventListener('click',_buscarTweets,false);
      //BOTONES NEXT Y BACK
      var nextButton=document.getElementById("btnNext");
      nextButton.addEventListener('click',_cargaVideosNext ,false);
       var befButton=document.getElementById("btnPrev");
      befButton.addEventListener('click',_cargarVideosBefore,false);
     
      //PUNTER EN MAPS
      var puntear=document.getElementById("searchMaps");
      puntear.addEventListener('click',_geolocalizacion,false);

      
    };









    
    return{
      "iniciar":_iniciar,
      "armarURLYoutube": _armarURLYoutube,
      "descargar": _descargar,
      //"enviaTraduccion": _enviaTraduccion,
      //"initMap":_initMap,
      //"procesarUbicacion":_procesarUbicacion,
      "geolocalizacion": _geolocalizacion,
      "armarURLVideo": _armarURLVideo
    
    };





})();

